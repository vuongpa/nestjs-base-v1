import { Injectable } from '@nestjs/common';
import { Types } from 'mongoose';
import { UserRepository } from './user/user.repository';
import { ADMIN_PERMISSION } from './permission/constants';

@Injectable()
export class AccessibilityService {
  constructor(private readonly userRepository: UserRepository) {}

  async getPermissionsCodeByUserId(userId: Types.ObjectId | string) {
    const user: any = await this.userRepository.findById(
      userId,
      {
        role_ids: 1,
        permission_ids: 1,
      },
      {
        populate: [
          {
            path: 'role_ids',
            select: { _id: 1 },
            populate: { path: 'permission_ids', select: { code: 1 } },
          },
          {
            path: 'group_permission_ids',
            select: { _id: 1 },
            populate: { path: 'permission_ids', select: { code: 1 } },
          },
          { path: 'permission_ids', select: { code: 1 } },
        ],
      },
    );
    const permissions = [...user.permission_ids];
    user.role_ids.forEach((role) => {
      permissions.push(...role.permission_ids);
    });
    user.group_permission_ids.forEach((groupPermission) => {
      permissions.push(...groupPermission.permission_ids);
    });
    return permissions.map((permission) => permission.code).filter(Boolean);
  }

  async isSupperAdmin(userId: Types.ObjectId | string) {
    const permissionCodes = await this.getPermissionsCodeByUserId(userId);
    return permissionCodes.includes(ADMIN_PERMISSION.SUPER_ADMIN);
  }

  async checkUserHasPermissions({
    permissionsOfUser,
    permissionsNeedCheck,
  }: {
    permissionsOfUser: string[];
    permissionsNeedCheck: string[];
  }) {
    return permissionsNeedCheck.every((permissionCode) => permissionsOfUser.includes(permissionCode));
  }
}
