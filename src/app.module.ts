import { Module, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { OrganizationModule } from './organization/organization.module';
import { PermissionModule } from './permission/permission.module';
import { RoleModule } from './role/role.module';
import * as Joi from 'joi';
import { AccessibilityService } from './accessibility.service';
import { CrudMiddleware, SearchMiddleware } from './middlewares';
import { BaseMiddleware } from './middlewares/base.middleware';
import { GroupPermissionModule } from './group-permission/group-permission.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRES_IN: Joi.string().required(),
        PORT: Joi.string().required(),
        REDIS_HOST: Joi.string().required(),
        REDIS_PORT: Joi.string().required(),
        MONGO_DB: Joi.string().required(),
        MONGO_USER: Joi.string().required(),
        MONGO_PASSWORD: Joi.string().required(),
        MONGO_URI: Joi.string().required(),
      }),
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    UserModule,
    OrganizationModule,
    PermissionModule,
    RoleModule,
    GroupPermissionModule,
  ],
  controllers: [AppController],
  providers: [AppService, AccessibilityService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(BaseMiddleware)
      .forRoutes('*')

      .apply(CrudMiddleware)
      .exclude('roles')
      .exclude('permissions')
      .exclude('group-permissions')
      .forRoutes('*')

      .apply(SearchMiddleware)
      .exclude('roles')
      .exclude('permissions')
      .exclude('group-permissions')
      .forRoutes('*');
  }
}
