import { Body, Controller, Get, Post, Put, Request, UseGuards } from '@nestjs/common';
import { LoginDto, UpdatePassword } from './auth.dto';
import { RefreshTokenGuard } from './guards/refreshToken.guard';
import { AccessTokenGuard } from './guards/accessToken.guard';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @UseGuards(RefreshTokenGuard)
  @Get('refresh')
  async refreshToken(@Request() request) {
    return this.authService.refreshJwtTokens(request.user.sub);
  }

  @UseGuards(AccessTokenGuard)
  @Get('/me')
  async getCurrentUser(@Request() request) {
    return this.authService.getCurrentUser(request.user.sub);
  }

  @UseGuards(AccessTokenGuard)
  @Put('/update-password')
  async resetPassword(@Request() request, @Body() requestBody: UpdatePassword) {
    await this.authService.updatePassword(request, requestBody);
  }

  @Post('/login')
  async login(@Body() requestBody: LoginDto) {
    return this.authService.login(requestBody);
  }

  // @UseGuards(AccessTokenGuard)
  @Post('/signup')
  async signup(@Body() body) {
    return this.authService.createUser(body);
  }
}
