import { IsEmail, IsString, Length } from 'class-validator';
import ValidatorUtil from '../utils/validator.util';

export class SignupDto {
  @IsEmail()
  email: string;

  @IsString()
  first_name: string;

  @IsString()
  last_name: string;

  @Length(6)
  password: string;

  @ValidatorUtil.isEqual('password', {
    message: 'Mật khẩu không khớp',
  })
  repeat_password: string;
}

export class UpdatePassword {
  @Length(6)
  old_password: string;

  @Length(6)
  new_password: string;

  @ValidatorUtil.isEqual('new_password', {
    message: 'Mật khẩu không khớp',
  })
  repeat_new_password: string;
}

export class LoginDto {
  @IsEmail()
  email: string;

  @Length(6)
  password: string;
}
