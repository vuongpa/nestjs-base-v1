import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { AuthController } from './auth.controller';
import { AccessTokenStrategy } from './strategies/accessToken.strategy';
import { RefreshTokenStrategy } from './strategies/refreshToken.strategy';
import { UserRepository } from '@/user/user.repository';
import { UserFactory } from '@/user/user.model';

@Module({
  controllers: [AuthController],
  providers: [AuthService, AccessTokenStrategy, RefreshTokenStrategy, UserRepository],
  imports: [JwtModule.register({}), MongooseModule.forFeature([UserFactory]), ConfigModule],
  exports: [AuthService],
})
export class AuthModule {}
