import { Test } from '@nestjs/testing';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AuthService } from './auth.service';
import { UserRepository } from '../user/user.repository';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { AccessTokenStrategy } from './strategies/accessToken.strategy';
import { RefreshTokenStrategy } from './strategies/refreshToken.strategy';

describe('AuthService', () => {
  let authService: AuthService;
  let jwtService: JwtService;
  let configService: ConfigService;
  let userRepository: UserRepository;
  const mockRepo = {
    create: () => Promise.resolve({}),
    findById: () => Promise.resolve({}),
    findOne: () => Promise.resolve({}),
    countDocuments: () => Promise.resolve(),
    find: () => Promise.resolve([]),
    updateMany: () => Promise.resolve(),
    updateOne: () => Promise.resolve(),
    aggregate: () => Promise.resolve([]),
    updateById: () => Promise.resolve({}),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [AuthService, AccessTokenStrategy, RefreshTokenStrategy, UserRepository, ConfigService, JwtService],
    })
      .overrideProvider(UserRepository)
      .useValue(mockRepo)
      .compile();

    authService = moduleRef.get<AuthService>(AuthService);
    jwtService = moduleRef.get<JwtService>(JwtService);
    configService = moduleRef.get<ConfigService>(ConfigService);
    userRepository = moduleRef.get<UserRepository>(UserRepository);
  });

  describe('updatePassword', () => {
    it('should update the password successfully', async () => {
      const request = {
        user: {
          sub: 'user-id',
        },
      };
      const oldPassword = 'old-password';
      const newPassword = 'new-password';
      const user: any = {
        _id: 'user-id',
        password: await bcrypt.hash('old-password', 10),
      };

      jest.spyOn(userRepository, 'findById').mockResolvedValue(user);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(true);
      jest.spyOn(bcrypt, 'hash').mockResolvedValue('hashed-new-password');
      jest.spyOn(userRepository, 'updateOne').mockResolvedValue(null);

      await authService.updatePassword(request, {
        old_password: oldPassword,
        new_password: newPassword,
        repeat_new_password: newPassword,
      });

      expect(userRepository.findById).toHaveBeenCalledWith('user-id');
      expect(bcrypt.compare).toHaveBeenCalledWith(oldPassword, user.password);
      expect(bcrypt.hash).toHaveBeenCalledWith(newPassword, 10);
      expect(userRepository.updateOne).toHaveBeenCalledWith('user-id', {
        password: 'hashed-new-password',
      });
    });

    it('should throw BadRequestException if the old password is incorrect', async () => {
      const request = {
        user: {
          sub: 'user-id',
        },
      };
      const oldPassword = 'old-password';
      const newPassword = 'new-password';
      const user: any = {
        _id: 'user-id',
        password: await bcrypt.hash('another-password', 10),
        email: 'test@example.com',
        first_name: 'first name',
        last_name: 'last name',
      };

      jest.spyOn(userRepository, 'findById').mockResolvedValue(user);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(false);

      await expect(
        authService.updatePassword(request, {
          old_password: oldPassword,
          new_password: newPassword,
          repeat_new_password: newPassword,
        }),
      ).rejects.toThrow(BadRequestException);

      expect(userRepository.findById).toHaveBeenCalledWith('user-id');
      expect(bcrypt.compare).toHaveBeenCalledWith(oldPassword, user.password);
    });
  });

  describe('generateJwtPayload', () => {
    it('should generate JWT payload', () => {
      const accessToken = 'access-token';
      const refreshToken = 'refresh-token';
      const expiresIn = 3600;

      jest.spyOn(configService, 'get').mockReturnValue(expiresIn);

      const result = authService.generateJwtPayload(accessToken, refreshToken);

      expect(configService.get).toHaveBeenCalledWith('JWT_EXPIRES_IN');
      expect(result).toEqual({
        accessToken,
        refreshToken,
        expiresIn,
      });
    });
  });

  describe('generateNewJwtTokens', () => {
    it('should generate new JWT tokens', async () => {
      const userId = 'user-id';
      const email = 'test@example.com';
      const accessToken = 'access-token';
      const refreshToken = 'refresh-token';

      jest.spyOn(jwtService, 'signAsync').mockResolvedValueOnce(accessToken);
      jest.spyOn(jwtService, 'signAsync').mockResolvedValueOnce(refreshToken);
      jest.spyOn(configService, 'get').mockReturnValueOnce('secret');
      jest.spyOn(configService, 'get').mockReturnValueOnce(3600);
      jest.spyOn(configService, 'get').mockReturnValueOnce('refresh-secret');
      jest.spyOn(configService, 'get').mockReturnValueOnce(7200);

      const result = await authService.generateNewJwtTokens(userId, email);

      expect(jwtService.signAsync).toHaveBeenNthCalledWith(
        1,
        { username: email, sub: userId },
        { secret: 'secret', expiresIn: '3600s' },
      );
      expect(jwtService.signAsync).toHaveBeenNthCalledWith(
        2,
        { username: email, sub: userId },
        { secret: 'refresh-secret', expiresIn: '7200s' },
      );
      expect(result).toEqual({
        accessToken,
        refreshToken,
      });
    });
  });

  describe('refreshJwtTokens', () => {
    it('should refresh JWT tokens', async () => {
      const userId = 'user-id';
      const user: any = {
        _id: userId,
        email: 'test@example.com',
      };
      const accessToken = 'access-token';
      const refreshToken = 'refresh-token';
      const expiresIn = 3600;

      jest.spyOn(userRepository, 'findById').mockResolvedValue(user);
      jest.spyOn(authService, 'generateNewJwtTokens').mockResolvedValue({
        accessToken,
        refreshToken,
      });
      jest.spyOn(configService, 'get').mockReturnValue(expiresIn);
      const result = await authService.refreshJwtTokens(userId);
      expect(configService.get).toHaveBeenCalledWith('JWT_EXPIRES_IN');

      expect(userRepository.findById).toHaveBeenCalledWith(userId);
      expect(authService.generateNewJwtTokens).toHaveBeenCalledWith(userId, user.email);
      expect(result).toEqual({
        accessToken,
        refreshToken,
        expiresIn: expect.any(Number),
        currentUser: expect.any(Object),
      });
    });
  });

  describe('getCurrentUser', () => {
    it('should get the current user', async () => {
      const userId = 'user-id';
      const user: any = {
        _id: userId,
      };

      jest.spyOn(userRepository, 'findById').mockResolvedValue(user);

      const result = await authService.getCurrentUser(userId);

      expect(userRepository.findById).toHaveBeenCalledWith(userId);
      expect(result).toEqual(user);
    });
  });

  describe('login', () => {
    it('should log in the user and return JWT tokens', async () => {
      const email = 'test@example.com';
      const password = 'password';
      const user: any = {
        email,
        password: await bcrypt.hash('password', 10),
        _id: 'id',
      };
      const accessToken = 'access-token';
      const refreshToken = 'refresh-token';
      const expiresIn = '3600';
      const refreshExpiresIn = '2952000';

      jest.spyOn(configService, 'get').mockReturnValue(expiresIn);
      jest.spyOn(configService, 'get').mockReturnValue(refreshExpiresIn);
      jest.spyOn(userRepository, 'findOne').mockResolvedValue(user);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(true);
      jest.spyOn(authService, 'generateNewJwtTokens').mockResolvedValue({
        accessToken,
        refreshToken,
      });

      const result = await authService.login({ email, password });

      expect(userRepository.findOne).toHaveBeenCalledWith({ email });
      expect(bcrypt.compare).toHaveBeenCalledWith(password, user.password);
      expect(authService.generateNewJwtTokens).toHaveBeenCalledWith(expect.any(String), email);
      expect(configService.get).toHaveBeenCalledWith('JWT_EXPIRES_IN');
      expect(result).toEqual({
        accessToken,
        refreshToken,
        expiresIn: expect.any(String),
        currentUser: expect.any(Object),
      });
    });

    it('should throw NotFoundException if the user is not found', async () => {
      const email = 'test@example.com';
      const password = 'password';

      jest.spyOn(userRepository, 'findOne').mockResolvedValue(null);

      await expect(authService.login({ email, password })).rejects.toThrow(NotFoundException);

      expect(userRepository.findOne).toHaveBeenCalledWith({ email });
    });

    it('should throw BadRequestException if the password is incorrect', async () => {
      const email = 'test@example.com';
      const password = 'password';
      const user: any = {
        email,
        password: await bcrypt.hash('another-password', 10),
      };

      jest.spyOn(userRepository, 'findOne').mockResolvedValue(user);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(false);

      await expect(authService.login({ email, password })).rejects.toThrow(BadRequestException);

      expect(userRepository.findOne).toHaveBeenCalledWith({ email });
      expect(bcrypt.compare).toHaveBeenCalledWith(password, user.password);
    });
  });
});
