import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { LoginDto, UpdatePassword } from './auth.dto';
import { omit } from 'lodash';
import { UserRepository } from '../user/user.repository';
import { ProjectionType, QueryOptions, Types } from 'mongoose';
import { User } from '@/user/user.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async updatePassword(request, { old_password, new_password }: UpdatePassword) {
    const user = await this.userRepository.findById(request.user.sub);
    const isPasswordMatching = await bcrypt.compare(old_password, user.password);
    if (!isPasswordMatching) {
      throw new BadRequestException('Mật khẩu không chính xác');
    }

    const newPassword = await bcrypt.hash(new_password, 10);
    await this.userRepository.updateOne(request.user.sub, {
      password: newPassword,
    });
  }

  generateJwtPayload(accessToken: string, refreshToken: string) {
    const expiresIn = this.configService.get<string>('JWT_EXPIRES_IN');
    return {
      accessToken,
      refreshToken,
      expiresIn,
    };
  }

  async generateNewJwtTokens(userId: Types.ObjectId | string, email: string) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        { username: email, sub: userId },
        {
          secret: this.configService.get('JWT_SECRET'),
          expiresIn: `${this.configService.get('JWT_EXPIRES_IN')}s`,
        },
      ),
      this.jwtService.signAsync(
        { username: email, sub: userId },
        {
          secret: this.configService.get('JWT_REFRESH_SECRET'),
          expiresIn: `${this.configService.get('JWT_REFRESH_EXPIRES_IN')}s`,
        },
      ),
    ]);

    return { accessToken, refreshToken };
  }

  async refreshJwtTokens(userId: string) {
    const currentUser = await this.userRepository.findById(userId);
    const { accessToken, refreshToken } = await this.generateNewJwtTokens(userId, currentUser.email);
    return {
      accessToken,
      refreshToken,
      expiresIn: this.configService.get<string>('JWT_EXPIRES_IN'),
      currentUser,
    };
  }

  async getCurrentUser(userId: string) {
    return this.userRepository.findById(userId);
  }

  async login({ email, password }: LoginDto) {
    const user: any = await this.userRepository.findOne({ email }, { password: 1 });
    if (!user) {
      throw new NotFoundException('Email không chính xác');
    }
    const isPasswordMatching = await bcrypt.compare(password, user.password);
    if (!isPasswordMatching) {
      throw new BadRequestException('Mật khẩu không chính xác');
    }
    const jwtTokens = await this.generateNewJwtTokens(user._id, user.email);
    const tokens = this.generateJwtPayload(jwtTokens.accessToken, jwtTokens.refreshToken);

    return {
      ...tokens,
      currentUser: omit(user, 'password'),
    };
  }

  async getCurrentUserFromToken(
    token: string,
    projection?: ProjectionType<User> | null,
    options?: QueryOptions<User> | null,
  ) {
    const secretKey = this.configService.get('JWT_SECRET');
    const jwtPayload = await this.jwtService.verifyAsync(token, {
      secret: secretKey,
    });
    return this.userRepository.findById(jwtPayload.sub, projection, options);
  }

  async createUser(data) {
    const passwordHash = await bcrypt.hash(data.password, 10);
    const user = await this.userRepository.create({
      ...data,
      password: passwordHash,
    });
    const { accessToken, refreshToken } = await this.generateNewJwtTokens(user._id, user.email);
    return {
      user,
      accessToken,
      refreshToken,
      expiresIn: this.configService.get<string>('JWT_EXPIRES_IN'),
    };
  }
}
