import { AppInstance } from '@/main';
import { UserRepository } from '@/user/user.repository';
import { ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@Injectable()
export class AccessTokenGuard extends AuthGuard('jwt') {
  constructor() {
    super();
  }

  async canActivate(context: ExecutionContext) {
    const configService: ConfigService = AppInstance.get(ConfigService);
    const jwtService: JwtService = AppInstance.get(JwtService);
    const userRepository: UserRepository = AppInstance.get(UserRepository);

    const request: any | Request = context.switchToHttp().getRequest();
    const token = request.headers.authorization?.split(' ')?.[1];
    if (!token) return false;

    let decodeToken;
    try {
      decodeToken = await jwtService.verifyAsync(token, { secret: configService.get<string>('JWT_SECRET') });
    } catch (err) {
      console.log('err access guard: ', err);
      return false;
    }

    if (!decodeToken.sub) return false;

    const currentUser = await userRepository.findById(decodeToken.sub);
    request.current_user = currentUser;
    return true;
  }
}
