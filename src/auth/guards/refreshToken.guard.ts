import { UserRepository } from '@/user/user.repository';
import { ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@Injectable()
export class RefreshTokenGuard extends AuthGuard('jwt-refresh') {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly userRepository: UserRepository,
  ) {
    super();
  }
  async canActivate(context: ExecutionContext) {
    const request: any | Request = context.switchToHttp().getRequest();
    const token = request.headers.authorization?.split(' ')?.[1];
    if (!token) return false;

    let decodeToken;
    try {
      decodeToken = await this.jwtService.verifyAsync(token, {
        secret: this.configService.get<string>('JWT_REFRESH_SECRET'),
      });
    } catch (err) {
      console.log('err refresh guard: ', err);
      return false;
    }

    if (!decodeToken.sub) return false;

    const currentUser = await this.userRepository.findById(decodeToken.sub);
    request.current_user = currentUser;
    return true;
  }
}
