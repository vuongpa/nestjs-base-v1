import { Get, Req, UseGuards } from '@nestjs/common';
import { BaseEntity } from './base.model';
import { BaseInterfaceController, OptionsController } from './base.interface.controller';
import { Request } from 'express';
import { AccessTokenGuard } from '@/auth/guards/accessToken.guard';
import { AllowedPermissions } from './decorators';
import { AllowedPermissionGuard } from './guards';
import { Permission } from '@/permission/permission.model';
import { BaseInterfaceRepository } from './base.interface.repository';

export function BaseController<T extends BaseEntity>(options: OptionsController) {
  abstract class BaseControllerClass implements BaseInterfaceController<T> {
    abstract defaultSearchFields: string[];
    abstract baseRepository: BaseInterfaceRepository<T>;

    async queryWithSearchCondition(req: Request | any) {
      let query = req.querymen.query || {};
      if (!req.query.q) return;

      const {
        query: queryWithSearch,
        cursor: cursorWithSearch,
        select: selectWithSearch,
      } = await this.baseRepository.buildQuerySearch(this.defaultSearchFields, req);
      const queryOr = query.$or;

      if (queryWithSearch.$or && queryOr) {
        const queryAnd = query.$and || [];
        queryAnd.push({ $or: queryOr });
        queryAnd.push({ $or: queryWithSearch.or });

        delete query.$or;

        query = { ...req.querymen.query, ...queryWithSearch, $and: queryAnd };
      } else {
        query = { ...req.querymen.query, ...queryWithSearch };
      }

      req.querymen = {
        query: queryWithSearch,
        cursor: { ...req.querymen.cursor, ...cursorWithSearch },
        select: { ...req.querymen.select, ...selectWithSearch },
      };
    }

    @AllowedPermissions(Permission.getSuggestionPermissions(options))
    @UseGuards(AccessTokenGuard, AllowedPermissionGuard)
    @Get()
    async findBase(@Req() req: Request | any) {
      await this.queryWithSearchCondition(req);

      const { query, select, cursor } = req.querymen;
      console.log(req.querymen, query);

      return this.baseRepository.find(query, select, cursor);
    }
  }
  return BaseControllerClass;
}
