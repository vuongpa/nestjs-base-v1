import { BaseEntity } from './base.model';

export interface OptionsController {
  SEARCH_PERMISSIONS_EXTRA?: [];
  SUGGESTION_PERMISSIONS_EXTRA?: [];
  VIEW_DETAIL_PERMISSIONS_EXTRA?: [];
  UPDATE_PERMISSIONS_EXTRA?: [];
  DELETE_PERMISSIONS_EXTRA?: [];
  CREATE_PERMISSIONS_EXTRA?: [];
  modelName: string;
}

export interface BaseInterfaceController<T extends BaseEntity> {}
