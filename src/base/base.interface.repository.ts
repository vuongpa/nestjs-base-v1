import {
  AggregateOptions,
  FilterQuery,
  PipelineStage,
  ProjectionType,
  QueryOptions,
  UpdateQuery,
  UpdateWithAggregationPipeline,
} from 'mongoose';
import { Request } from 'express';

export interface BaseInterfaceRepository<T> {
  create(dto: T | any): Promise<T>;
  findById(id: string, projection?: ProjectionType<T> | null, options?: QueryOptions<T> | null);
  findOne(filter?: FilterQuery<T>, projection?: ProjectionType<T> | null, options?: QueryOptions<T> | null);
  countDocuments(filter?: FilterQuery<T>, options?: QueryOptions<T>);
  find(
    filter?: FilterQuery<T>,
    projection?: ProjectionType<T> | null | undefined,
    options?: QueryOptions<T> | null | undefined,
  );
  updateMany(
    filter?: FilterQuery<T>,
    update?: UpdateQuery<T> | UpdateWithAggregationPipeline,
    options?: QueryOptions<T> | null,
  );
  updateOne(
    filter?: FilterQuery<T>,
    update?: UpdateQuery<T> | UpdateWithAggregationPipeline,
    options?: QueryOptions<T> | null,
  );
  aggregate(pipeline?: PipelineStage[], options?: AggregateOptions);
  updateById(id: string, update?: UpdateQuery<T> | UpdateWithAggregationPipeline, options?: QueryOptions<T> | null);
  buildQuerySearch(fields: string[], req: Request | any);
}
