import { Prop, SchemaOptions } from '@nestjs/mongoose';
import { Schema, Document } from 'mongoose';

export enum STATUS {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export abstract class BaseEntity extends Document {
  @Prop()
  deleted_at: Date;
  org_id: Schema.Types.ObjectId;
  root_org_id: Schema.Types.ObjectId;
  created_by_id: Schema.Types.ObjectId;
  deleted_by_id: Schema.Types.ObjectId;
  updated_by_id: Schema.Types.ObjectId;
  sharing_with_org_ids: [Schema.Types.ObjectId];
  sharing_with_user_ids: [Schema.Types.ObjectId];
}

export const schemaOptionsBase: SchemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
};
