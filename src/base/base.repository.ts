import { BaseEntity } from './base.model';
import { BaseInterfaceRepository } from './base.interface.repository';
import {
  AggregateOptions,
  FilterQuery,
  Model,
  PipelineStage,
  ProjectionType,
  QueryOptions,
  Types,
  UpdateQuery,
  UpdateWithAggregationPipeline,
} from 'mongoose';
import { convertToObjectId } from '../utils';
import { Request } from 'express';

export abstract class BaseRepository<T extends BaseEntity> implements BaseInterfaceRepository<T> {
  protected constructor(private readonly model: Model<T>) {
    this.model = model;
  }

  async aggregate(pipeline?: PipelineStage[], options?: AggregateOptions) {
    return this.model.aggregate(pipeline, options);
  }

  async countDocuments(filter?: FilterQuery<T>, options?: QueryOptions<T>) {
    return this.model.countDocuments(filter, options);
  }

  async create(dto: any | any[]) {
    const createdData = await this.model.create(dto);
    return createdData.toObject({ getters: true });
  }

  async find(
    filter?: FilterQuery<T>,
    projection?: ProjectionType<T> | null | undefined,
    options?: QueryOptions<T> | null | undefined,
  ) {
    return this.model.find(filter, projection, options).lean();
  }

  async findById(id: Types.ObjectId | string, projection?: ProjectionType<T> | null, options?: QueryOptions<T> | null) {
    return this.model.findById(id, projection, options).lean();
  }

  async findOne(filter?: FilterQuery<T>, projection?: ProjectionType<T> | null, options?: QueryOptions<T> | null) {
    return this.model.findOne(filter, projection, options).lean();
  }

  async updateMany(
    filter?: FilterQuery<T>,
    update?: UpdateQuery<T> | UpdateWithAggregationPipeline,
    options?: QueryOptions<T> | null,
  ) {
    return this.model.updateMany(filter, update, options).lean();
  }

  async updateOne(
    filter?: FilterQuery<T>,
    update?: UpdateQuery<T> | UpdateWithAggregationPipeline,
    options?: QueryOptions<T> | null,
  ) {
    return this.model.updateOne(filter, update, options).lean();
  }

  async updateById(
    id: Types.ObjectId | string,
    update?: UpdateQuery<T> | UpdateWithAggregationPipeline,
    options?: QueryOptions<T> | null,
  ) {
    return this.updateOne({
      _id: convertToObjectId(id),
      update,
      options,
    });
  }

  async buildQuerySearch(fields: string[], request: Request | any) {
    if (!fields || !fields.length) return {};
    const search = request.query.q;
    if (!search) return {};
    const conditions: any = [];
    let cursor: any = {};
    let select: any = {};
    const query: any = {};
    const isFieldsHasTextIndex = await this.checkFieldsHasTextIndex(fields);
    if (isFieldsHasTextIndex) {
      cursor = {
        sort: { ...request.querymen.cursor.sort, score: { $meta: 'textScore' } },
      };
      select = { score: { $meta: 'textScore' } };
      conditions.push({ $text: { $search: search } });
    }
    for (const field of fields) {
      const isFieldHasTextIndex = await this.checkFieldHasTextIndex(field);
      if (!isFieldHasTextIndex) {
        conditions.push({ [field]: new RegExp(search, 'i') });
      }
    }
    if (conditions.length) {
      query.$or = conditions;
    }
    return { query, select, cursor };
  }

  async checkFieldHasTextIndex(fileName: string) {
    const indexes = await this.model.collection.getIndexes();
    const indexKeys = Object.keys(indexes);
    return indexKeys.includes(`${fileName}_text`);
  }

  async checkFieldsHasTextIndex(fileNames: string[]) {
    const indexes = await this.model.collection.getIndexes();
    const indexKeys = Object.keys(indexes);
    return fileNames.some((fileName) => indexKeys.includes(`${fileName}_text`));
  }
}
