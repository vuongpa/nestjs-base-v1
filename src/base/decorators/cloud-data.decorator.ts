import { ExecutionContext, createParamDecorator } from '@nestjs/common';

export const CloudData = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  return ctx.switchToHttp().getRequest().headers.cloudData;
});
