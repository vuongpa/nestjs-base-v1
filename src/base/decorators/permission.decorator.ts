import { SetMetadata } from '@nestjs/common';

export const AllowedPermissions = (permissions: string[] = []) => SetMetadata('permissions', permissions);
