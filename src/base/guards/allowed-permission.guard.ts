import { AccessibilityService } from '@/accessibility.service';
import { AppInstance } from '@/main';
import { ADMIN_PERMISSION } from '@/permission/constants';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';

@Injectable()
export class AllowedPermissionGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  async canActivate(context: ExecutionContext) {
    const permissions = this.reflector.get<string[]>('permissions', context.getHandler());
    if (!permissions || !permissions.length) return true;

    const request: Request | any = context.switchToHttp().getRequest();
    const currentUser = request.headers.cloudData.current_user;
    const accessibilityService: AccessibilityService = AppInstance.get(AccessibilityService);
    const permissionCodesOfUser = await accessibilityService.getPermissionsCodeByUserId(currentUser._id);

    if (permissionCodesOfUser.includes(ADMIN_PERMISSION.SUPER_ADMIN)) {
      return true;
    }

    const hasAccess = await accessibilityService.checkUserHasPermissions({
      permissionsOfUser: permissionCodesOfUser,
      permissionsNeedCheck: permissions,
    });
    return hasAccess;
  }
}
