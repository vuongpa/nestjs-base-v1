import { Controller } from '@nestjs/common';
import { GroupPermissionService } from './group-permission.service';

@Controller('group-permissions')
export class GroupPermissionController {
  constructor(private readonly groupPermissionService: GroupPermissionService) {}
}
