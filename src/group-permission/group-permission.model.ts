import { BaseEntity, schemaOptionsBase } from '@/base/base.model';
import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

const COLLECTION_NAME = 'group-permissions';

export type GroupPermissionDocument = HydratedDocument<GroupPermission>;

@Schema({ ...schemaOptionsBase, collection: COLLECTION_NAME })
export class GroupPermission extends BaseEntity {
  @Prop({ required: true, type: String })
  code: string;

  @Prop({ required: true })
  name: string;

  @Prop()
  des: string;

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Permission' }],
    default: [],
  })
  permission_ids: mongoose.Schema.Types.ObjectId[];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  created_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  deleted_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  updated_by_id: mongoose.Schema.Types.ObjectId;
}

export const GroupPermissionSchema = SchemaFactory.createForClass(GroupPermission);
GroupPermissionSchema.index({ name: 'text' });
GroupPermissionSchema.index({ code: 1 });
GroupPermissionSchema.index({ created_at: 1 });
GroupPermissionSchema.index({ created_by_id: 1 });

export const GroupPermissionFactory: ModelDefinition = {
  name: GroupPermission.name,
  schema: GroupPermissionSchema,
};
