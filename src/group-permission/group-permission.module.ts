import { Module } from '@nestjs/common';
import { GroupPermissionService } from './group-permission.service';
import { GroupPermissionController } from './group-permission.controller';
import { GroupPermissionRepository } from './group-permission.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupPermissionFactory } from './group-permission.model';

@Module({
  imports: [MongooseModule.forFeature([GroupPermissionFactory])],
  controllers: [GroupPermissionController],
  providers: [GroupPermissionService, GroupPermissionRepository],
  exports: [GroupPermissionService, GroupPermissionRepository],
})
export class GroupPermissionModule {}
