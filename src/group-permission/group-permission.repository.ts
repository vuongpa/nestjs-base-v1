import { BaseRepository } from '@/base/base.repository';
import { Injectable } from '@nestjs/common';
import { GroupPermission } from './group-permission.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class GroupPermissionRepository extends BaseRepository<GroupPermission> {
  constructor(
    @InjectModel(GroupPermission.name)
    private readonly groupPermissionModel: Model<GroupPermission>,
  ) {
    super(groupPermissionModel);
  }
}
