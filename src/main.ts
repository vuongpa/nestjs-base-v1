import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import helmet from 'helmet';
import * as compression from 'compression';
import { ValidationPipe } from '@nestjs/common';
import { middleware as query } from 'querymen';
import { syncModelPermission } from './migrations/permission.migrate';
import { InitializationSystem } from './migrations';
import { OrganizationRepository } from './organization/organization.repository';
import { RoleRepository } from './role/role.repository';
import { UserRepository } from './user/user.repository';
import { PermissionRepository } from './permission/permission.repository';

let AppInstance;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  AppInstance = app;

  const apiVersion = process.env.API_VERSION || 'v1';
  const port = process.env.PORT || 3000;

  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix(`api/${apiVersion}`);
  app.use(helmet(), compression(), query());

  executeInitializationSystem();

  await app.listen(port);
}
bootstrap();

const executeInitializationSystem = (app = AppInstance) => {
  const organizationRepository: OrganizationRepository = app.get(OrganizationRepository);
  const userRepository: UserRepository = app.get(UserRepository);
  const roleRepository: RoleRepository = app.get(RoleRepository);
  const permissionRepository: PermissionRepository = app.get(PermissionRepository);

  const initializationSystem = new InitializationSystem(
    organizationRepository,
    userRepository,
    roleRepository,
    permissionRepository,
  );
  initializationSystem.execute();

  syncModelPermission(app);
};

export { AppInstance };
