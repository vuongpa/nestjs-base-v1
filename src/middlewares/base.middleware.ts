import { AuthService } from '@/auth/auth.service';
import { AppInstance } from '@/main';
import { User } from '@/user/user.model';
import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as mongodb from 'mongodb';
import { Types } from 'mongoose';

const ObjectId = mongodb.ObjectId;

export interface CloudData {
  org_id: Types.ObjectId | string;
  root_org_id: Types.ObjectId | string;
  current_user: User;
}

@Injectable()
export class BaseMiddleware implements NestMiddleware {
  async use(req: Request | any, res: Response, next: NextFunction) {
    const authService: AuthService = AppInstance.get(AuthService);
    delete req?.querymen?.query?.keywords;

    const accessToken = req.headers.authorization?.split(' ')?.[1];
    if (!accessToken) return next();
    let currentUser;
    try {
      currentUser = await authService.getCurrentUserFromToken(accessToken);
    } catch (err) {
      throw new UnauthorizedException('Phiên đăng nhập đã hết hạn');
    }
    req.headers.cloudData = {
      current_user: currentUser,
      root_org_id: currentUser.root_org_id || new ObjectId(),
      org_id: currentUser.org_id || new ObjectId(),
    };
    return next();
  }
}
