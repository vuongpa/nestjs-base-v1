import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { CloudData } from './base.middleware';
import { AccessibilityService } from '@/accessibility.service';
import { AppInstance } from '@/main';

@Injectable()
export class CrudMiddleware implements NestMiddleware {
  use(req: any | Request, res: Response, next: NextFunction) {
    if (!req.headers.authorization) return next();

    const accessibilityService: AccessibilityService = AppInstance.get(AccessibilityService);
    const currentUser = (req.headers?.cloudData as unknown as CloudData)?.current_user;
    const isSupperAdmin = accessibilityService.isSupperAdmin(currentUser._id);
    if (isSupperAdmin) return next();

    const { isCreating, isUpdating } = CrudMiddleware.getRequestActionType(req);
    if (isCreating) {
      CrudMiddleware.attachCloudDataWhenCreate(req);
    }
    if (isUpdating) {
      CrudMiddleware.attachCloudDataWhenUpdate(req);
    }

    return next();
  }

  static getRequestActionType(request: Request | any) {
    const isUpdating = ['put', 'patch'].includes(request.method.toLowerCase());
    const isCreating = ['post'].includes(request.method.toLowerCase());
    const isDeleting = ['delete'].includes(request.method.toLowerCase());
    const isSearching = !isCreating && !isUpdating && !isDeleting;
    return { isUpdating, isCreating, isDeleting, isSearching };
  }

  static attachCloudDataWhenCreate(request: Request) {
    const cloudData: CloudData = request.headers.cloudData as unknown as CloudData;
    if (request.body) {
      request.body.org_id = cloudData.current_user.org_id;
      request.body.root_org_id = cloudData.current_user.root_org_id;
      request.body.created_by_id = cloudData.current_user._id;
    }
  }

  static attachCloudDataWhenUpdate(request: Request) {
    const cloudData: CloudData = request.headers.cloudData as unknown as CloudData;
    if (request.body) {
      request.body.org_id = cloudData.current_user.org_id;
      request.body.root_org_id = cloudData.current_user.root_org_id;
      request.body.updated_by_id = cloudData.current_user._id;
    }
  }
}
