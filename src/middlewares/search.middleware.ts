import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request } from 'express';
import { CrudMiddleware } from './crud.middleware';
import { CloudData } from './base.middleware';
import { convertToObjectId } from '@/utils';
import { AppInstance } from '@/main';
import { AccessibilityService } from '@/accessibility.service';

@Injectable()
export class SearchMiddleware implements NestMiddleware {
  use(req: Request | any, res: Response, next: NextFunction) {
    if (!req.headers.authorization) return next();
    const { isSearching } = CrudMiddleware.getRequestActionType(req);
    if (!isSearching) return next();
    const accessibilityService: AccessibilityService = AppInstance.get(AccessibilityService);
    const currentUser = (req.headers?.cloudData as unknown as CloudData)?.current_user;
    const isSupperAdmin = accessibilityService.isSupperAdmin(currentUser._id);
    if (isSupperAdmin) return next();
    SearchMiddleware.modifyOrgQuery(req);
    SearchMiddleware.modifyRootOrgQuery(req);
    return next();
  }

  static modifyOrgQuery(request: any | Request) {
    const cloudData: CloudData = request.headers.cloudData as unknown as CloudData;
    const org_id = cloudData.org_id;
    const orgQuery = request.querymen.org_id;
    if (typeof orgQuery === 'object') {
      if (orgQuery['$in']) {
        request.querymen.query.org_id['$in'] = [...request.querymen.query.org_id['$in'], convertToObjectId(org_id)];
      }
      if (!orgQuery['$in']) {
        request.querymen.query.org_id['$in'] = [convertToObjectId(org_id)];
      }
    }

    if (typeof orgQuery === 'string') {
      if (!orgQuery['$in']) {
        request.querymen.query.org_id['$in'] = [convertToObjectId(org_id), convertToObjectId(orgQuery)];
      }
    }

    if (!orgQuery) {
      request.querymen.query.org_id = {
        $in: [convertToObjectId(org_id)],
      };
    }
  }

  static modifyRootOrgQuery(req: Request | any) {
    const cloudData: CloudData = req.headers.cloudData as unknown as CloudData;
    req.querymen.query.root_org_id = cloudData.root_org_id;
  }
}
