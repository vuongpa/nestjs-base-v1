import { OrganizationRepository } from '@/organization/organization.repository';
import { PermissionRepository } from '@/permission/permission.repository';
import { RoleRepository } from '@/role/role.repository';
import { UserRepository } from '@/user/user.repository';
import { Injectable } from '@nestjs/common';
import { readFileSync, readdirSync } from 'fs';
import { join } from 'path';
import * as YAML from 'yaml';
import * as bcrypt from 'bcrypt';

@Injectable()
export class InitializationSystem {
  constructor(
    private readonly organizationRepository: OrganizationRepository,
    private readonly userRepository: UserRepository,
    private readonly roleRepository: RoleRepository,
    private readonly permissionRepository: PermissionRepository,
  ) {}

  async getObjDataFromRelativePath(pathUrl: string) {
    const dir = join(__dirname, pathUrl);
    const files = readdirSync(dir);
    return files.map((file) => {
      const sourceFile = join(dir, file);
      return YAML.parse(readFileSync(sourceFile, 'utf8'));
    });
  }

  async initUsers() {
    const data = await this.getObjDataFromRelativePath('../../src/fixtures/users');
    data.forEach(async (user) => {
      const existsUser = await this.userRepository.findByEmail(user.email);
      if (!existsUser) {
        const permissions = await this.permissionRepository.find({ code: { $in: user.permission_codes } }, { _id: 1 });
        const passwordHash = await bcrypt.hash(user.password.toString(), 10);
        this.userRepository.create({
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          permission_ids: permissions.map((p) => p._id),
          password: passwordHash,
        });
      }
    });
  }

  async initOrgs() {
    const data = await this.getObjDataFromRelativePath('../../src/fixtures/orgs');
    data.forEach(async (org) => {
      const existsOrg = await this.organizationRepository.findByCode(org.code);
      if (!existsOrg) {
        let rootOrg;
        if (org.rootCode) {
          rootOrg = await this.organizationRepository.findByCode(org.rootCode, {
            _id: 1,
          });
        }
        this.organizationRepository.create({
          name: org.name,
          code: org.code,
          root_org_id: rootOrg?._id || null,
        });
      }
    });
  }

  async initRoles() {}

  async initPermissions() {
    const data = await this.getObjDataFromRelativePath('../../src/fixtures/permissions');
    data.forEach(async (permission) => {
      const existsPermission = await this.permissionRepository.findByCode(permission.code);
      if (!existsPermission) {
        this.permissionRepository.create({
          name: permission.name,
          code: permission.code,
          type: permission.type,
        });
      }
    });
  }

  async execute() {
    await this.initOrgs();
    await this.initRoles();
    await this.initPermissions();
    await this.initUsers();
  }
}
