import { AppInstance } from '@/main';
import { PermissionService } from '@/permission/permission.service';
import { generatePermissionsOnModel } from '@/permission/utils';
import { getModelNames } from '@/utils';

export const IGNORE_MODELS = [];

export const syncModelPermission = async (app = AppInstance) => {
  const modelNames = getModelNames();
  const permissionService: PermissionService = app.get(PermissionService);
  if (!permissionService) {
    console.error('Could not found permission repository');
  }
  for (const modelName of modelNames) {
    if (IGNORE_MODELS.includes(modelName)) break;
    Promise.all(
      Object.values(generatePermissionsOnModel(modelName)).map((code) => {
        return permissionService.insertModelPermission(code);
      }),
    );
  }
};
