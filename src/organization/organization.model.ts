import { schemaOptionsBase, BaseEntity } from '@/base/base.model';
import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type OrganizationDocument = HydratedDocument<Organization>;
const COLLECTION_NAME = 'organizations';

@Schema({
  ...schemaOptionsBase,
  collection: COLLECTION_NAME,
})
export class Organization extends BaseEntity {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  root_org_id: mongoose.Schema.Types.ObjectId;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  org_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  created_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  deleted_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  updated_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ unique: true, required: true, index: true, trim: true })
  code: string;

  @Prop({ required: true, trim: true })
  name: string;

  @Prop()
  avatar: string;

  @Prop()
  address: string;
}

export const OrganizationSchema = SchemaFactory.createForClass(Organization);
OrganizationSchema.index({ name: 'text' });
OrganizationSchema.index({ root_org_id: 1, org_id: 1 });
OrganizationSchema.index({ created_at: 1 });
OrganizationSchema.index({ created_by_id: 1 });
OrganizationSchema.index({ code: 1 });

export const OrganizationFactory: ModelDefinition = {
  name: 'Organization',
  schema: OrganizationSchema,
};
