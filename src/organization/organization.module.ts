import { Module } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { OrganizationRepository } from './organization.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { OrganizationFactory } from './organization.model';

@Module({
  controllers: [OrganizationController],
  providers: [OrganizationService, OrganizationRepository],
  imports: [MongooseModule.forFeature([OrganizationFactory])],
})
export class OrganizationModule {}
