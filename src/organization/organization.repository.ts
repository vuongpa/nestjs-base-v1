import { Injectable } from '@nestjs/common';
import { BaseRepository } from '../base/base.repository';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ProjectionType, QueryOptions } from 'mongoose';
import { Organization } from './organization.model';

@Injectable()
export class OrganizationRepository extends BaseRepository<Organization> {
  constructor(
    @InjectModel(Organization.name)
    private readonly organizationModel: Model<Organization>,
  ) {
    super(organizationModel);
  }
  async findByCode(
    code: string,
    projection?: ProjectionType<Organization> | null,
    options?: QueryOptions<Organization> | null,
  ) {
    return this.organizationModel.findOne({ code }, projection, options);
  }
}
