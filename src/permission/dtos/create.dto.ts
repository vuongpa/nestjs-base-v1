import { IsString } from 'class-validator';

export class RequestCreatePermissionDto {
  @IsString()
  code: string;
  name: string;
  description: string;
  @IsString()
  type: string;
}
