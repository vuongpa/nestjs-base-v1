import { Controller } from '@nestjs/common';
import { BaseController } from '@/base/base.controller';
import { Permission } from './permission.model';
import { OptionsController } from '@/base/base.interface.controller';
import { BaseInterfaceRepository } from '@/base/base.interface.repository';
import { PermissionRepository } from './permission.repository';

const options: OptionsController = { modelName: Permission.name };
@Controller('permissions')
export class PermissionController extends BaseController<Permission>(options) {
  baseRepository: BaseInterfaceRepository<Permission>;
  defaultSearchFields: string[] = ['code', 'name'];

  constructor(private readonly permissionRepository: PermissionRepository) {
    super();
    this.baseRepository = this.permissionRepository;
  }
}
