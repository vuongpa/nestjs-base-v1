import { BaseEntity, schemaOptionsBase } from '../base/base.model';
import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { OptionsController } from '@/base/base.interface.controller';
import {
  generateViewMyDocumentOnlyPermission,
  generateAllViewPermissions,
  generateSuggestionPermissions,
  generateViewDetailPermissions,
  generateUpdatePermission,
  generateDeletePermission,
  generateCreatePermission,
} from './utils';

export type PermissionDocument = HydratedDocument<Permission>;

export enum PERMISSION_TYPE {
  MODEL_PERMISSION = 'model_permission',
  SYS_PERMISSION = 'system_permission',
}

export const COLLECTION_NAME = 'permissions';

@Schema({ ...schemaOptionsBase, collection: COLLECTION_NAME })
export class Permission extends BaseEntity {
  @Prop({ required: true, unique: true })
  code: string;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop()
  type: PERMISSION_TYPE;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  created_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  deleted_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  updated_by_id: mongoose.Schema.Types.ObjectId;

  static getSearchPermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const viewPermissionsExtra = options.SEARCH_PERMISSIONS_EXTRA || [];
    const viewMyDocumentOnly = Object.values(generateViewMyDocumentOnlyPermission(modelName));
    return [...Object.values(generateAllViewPermissions(modelName)), ...viewPermissionsExtra, ...viewMyDocumentOnly];
  };

  static getSuggestionPermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const suggestionPermissionsExtra = options.SUGGESTION_PERMISSIONS_EXTRA || [];
    const viewMyDocumentOnly = Object.values(generateViewMyDocumentOnlyPermission(modelName));
    return [
      ...Object.values(generateSuggestionPermissions(modelName)),
      ...suggestionPermissionsExtra,
      ...viewMyDocumentOnly,
    ];
  };

  static getViewDetailPermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const viewDetailPermissionsExtra = options.VIEW_DETAIL_PERMISSIONS_EXTRA || [];
    return [...Object.values(generateViewDetailPermissions(modelName)), ...viewDetailPermissionsExtra];
  };

  static getUpdatePermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const updatePermissionsExtra = options.UPDATE_PERMISSIONS_EXTRA || [];
    return [...Object.values(generateUpdatePermission(modelName)), ...updatePermissionsExtra];
  };
  static getDeletePermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const deletePermissionsExtra = options?.DELETE_PERMISSIONS_EXTRA || [];
    return [...Object.values(generateDeletePermission(modelName)), ...deletePermissionsExtra];
  };
  static getCreatePermissions = (options: OptionsController): string[] => {
    const modelName = options.modelName;
    const createPermissionsExtra = options?.CREATE_PERMISSIONS_EXTRA || [];
    return [...Object.values(generateCreatePermission(modelName)), ...createPermissionsExtra];
  };

  static getViewMyDocumentOnly(options: OptionsController): string {
    return Object.values(generateViewMyDocumentOnlyPermission(options.modelName))[0];
  }
}

export const PermissionSchema = SchemaFactory.createForClass(Permission);
PermissionSchema.index({ name: 'text' });
PermissionSchema.index({ code: 1 });
PermissionSchema.index({ created_at: 1 });
PermissionSchema.index({ created_by_id: 1 });

export const PermissionFactory: ModelDefinition = {
  name: Permission.name,
  schema: PermissionSchema,
};
