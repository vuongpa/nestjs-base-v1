import { Module } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { PermissionController } from './permission.controller';
import { PermissionRepository } from './permission.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { PermissionFactory } from './permission.model';

@Module({
  imports: [MongooseModule.forFeature([PermissionFactory])],
  controllers: [PermissionController],
  providers: [PermissionService, PermissionRepository],
  exports: [PermissionRepository, PermissionService],
})
export class PermissionModule {}
