import { Injectable } from '@nestjs/common';
import { Permission } from './permission.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ProjectionType, QueryOptions } from 'mongoose';
import { BaseRepository } from '../base/base.repository';

@Injectable()
export class PermissionRepository extends BaseRepository<Permission> {
  constructor(
    @InjectModel(Permission.name)
    private readonly permissionModel: Model<Permission>,
  ) {
    super(permissionModel);
  }

  async findByCode(
    code: string,
    projection?: ProjectionType<Permission> | null,
    options?: QueryOptions<Permission> | null,
  ) {
    return this.permissionModel.findOne({ code }, projection, options);
  }
}
