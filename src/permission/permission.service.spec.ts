import { Test, TestingModule } from '@nestjs/testing';
import { PermissionService } from './permission.service';
import { PermissionRepository } from './permission.repository';
import { PERMISSION_TYPE } from './permission.model';

describe('PermissionService', () => {
  let permissionService: PermissionService;
  let permissionRepository: PermissionRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PermissionService,
        {
          provide: PermissionRepository,
          useValue: {
            findByCode: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    permissionService = module.get<PermissionService>(PermissionService);
    permissionRepository = module.get<PermissionRepository>(PermissionRepository);
  });

  describe('insertModelPermission', () => {
    it('should create a new permission when the permission model does not exist', async () => {
      // Mock data
      const code = 'example_code';
      const permissionModel = null; // Simulate non-existing permission model

      // Mock the findByCode method of permissionRepository
      jest.spyOn(permissionRepository, 'findByCode').mockResolvedValue(permissionModel);

      // Call the insertModelPermission method
      await permissionService.insertModelPermission(code);

      // Assert that the findByCode method was called with the correct arguments
      expect(permissionRepository.findByCode).toHaveBeenCalledWith(code, {
        _id: true,
      });

      // Assert that the create method was called with the correct arguments
      expect(permissionRepository.create).toHaveBeenCalledWith({
        code,
        type: PERMISSION_TYPE.MODEL_PERMISSION,
      });
    });
  });
});
