import { Injectable } from '@nestjs/common';
import { PermissionRepository } from './permission.repository';
import { PERMISSION_TYPE } from './permission.model';

@Injectable()
export class PermissionService {
  constructor(private readonly permissionRepository: PermissionRepository) {}

  async insertModelPermission(code: string, permissionRepository: PermissionRepository = this.permissionRepository) {
    const permissionModel = await permissionRepository.findByCode(code, {
      _id: true,
    });
    if (!permissionModel) {
      await this.permissionRepository.create({
        code,
        type: PERMISSION_TYPE.MODEL_PERMISSION,
      });
    }
  }
}
