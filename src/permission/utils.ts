export interface IModelPermission {
  CREATE: string;
  UPDATE: string;
  DELETE: string;
  VIEW_ALL: string;
  VIEW_DETAIL: string;
  VIEW_MY_DATA_ONLY: string;
  VIEW_SUGGESTION: string;
}

export const generateAllViewPermissions = (modelName: string) => ({
  VIEW_ALL: `V_${modelName.toUpperCase()}`,
});

export const generateSuggestionPermissions = (modelName: string) => ({
  VIEW_SUGGESTION: `S_${modelName.toUpperCase()}`,
});

export const generateViewDetailPermissions = (modelName: string) => ({
  VIEW_DETAIL: `V_D_${modelName.toUpperCase()}`,
});

export const generateViewMyDocumentOnlyPermission = (modelName: string) => ({
  VIEW_MY_DATA_ONLY: `V_MDT_${modelName.toUpperCase()}`,
});

export const generateUpdatePermission = (modelName: string) => ({
  UPDATE: `U_${modelName.toUpperCase()}`,
});

export const generateDeletePermission = (modelName: string) => ({
  DELETE: `D_${modelName.toUpperCase()}`,
});

export const generateCreatePermission = (modelName: string) => ({
  CREATE: `C_${modelName.toUpperCase()}`,
});

export const generatePermissionsOnModel = (modelName: string): IModelPermission => ({
  ...generateAllViewPermissions(modelName),
  ...generateSuggestionPermissions(modelName),
  ...generateViewDetailPermissions(modelName),
  ...generateViewMyDocumentOnlyPermission(modelName),
  ...generateUpdatePermission(modelName),
  ...generateDeletePermission(modelName),
  ...generateCreatePermission(modelName),
});
