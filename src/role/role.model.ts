import { schemaOptionsBase, BaseEntity, STATUS } from '@/base/base.model';
import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

const COLLECTION_NAME = 'roles';

export type RoleDocument = HydratedDocument<Role>;

@Schema({ ...schemaOptionsBase, collection: COLLECTION_NAME })
export class Role extends BaseEntity {
  @Prop({ required: true })
  status: STATUS;

  @Prop({ required: true, minlength: 2, maxlength: 100 })
  name: string;

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Permission' }],
    default: [],
  })
  permission_ids: mongoose.Schema.Types.ObjectId[];

  @Prop()
  des: string;

  @Prop({ required: true })
  code: string;

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'GroupPermission' }],
  })
  group_permission_ids: mongoose.Schema.Types.ObjectId[];

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  root_org_id: mongoose.Schema.Types.ObjectId;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  org_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  created_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  deleted_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  updated_by_id: mongoose.Schema.Types.ObjectId;
}

export const RoleSchema = SchemaFactory.createForClass(Role);
RoleSchema.index({ name: 'text' });
RoleSchema.index({ root_org_id: 1, org_id: 1 });
RoleSchema.index({ created_at: 1 });
RoleSchema.index({ created_by_id: 1 });
RoleSchema.index({ code: 1 });

export const RoleFactory: ModelDefinition = {
  name: Role.name,
  schema: RoleSchema,
};
