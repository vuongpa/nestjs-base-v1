import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from '@/base/base.repository';
import { Role } from './role.model';

@Injectable()
export class RoleRepository extends BaseRepository<Role> {
  constructor(
    @InjectModel(Role.name)
    private readonly roleModel: Model<Role>,
  ) {
    super(roleModel);
  }
}
