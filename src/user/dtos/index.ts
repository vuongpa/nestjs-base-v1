import { IsEmail, IsString } from 'class-validator';

export class RequestCreateUserDto {
  @IsString()
  first_name: string;

  @IsString()
  last_name: string;

  @IsEmail()
  email: string;

  @IsString()
  password: string;

  phone_number: string;
}
