import { Controller, Get, Req } from '@nestjs/common';
import { UserRepository } from './user.repository';

@Controller('users')
export class UserController {
  constructor(private readonly userRepository: UserRepository) {}

  @Get()
  get(@Req() req) {
    const { query, select, cursor } = req.querymen;
    return this.userRepository.find(query, select, cursor);
  }
}
