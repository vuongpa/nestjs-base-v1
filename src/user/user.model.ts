import { schemaOptionsBase, BaseEntity } from '../base/base.model';
import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;
const COLLECTION_NAME = 'users';

export enum GENDER {
  Male = 'MALE',
  Female = 'FEMALE',
  Other = 'OTHER',
}

@Schema({
  ...schemaOptionsBase,
  collection: COLLECTION_NAME,
})
export class User extends BaseEntity {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  root_org_id: mongoose.Schema.Types.ObjectId;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
  })
  org_id: mongoose.Schema.Types.ObjectId;

  @Prop({
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Organization',
      },
    ],
  })
  joined_org_ids: mongoose.Schema.Types.ObjectId[];

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  created_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  deleted_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  updated_by_id: mongoose.Schema.Types.ObjectId;

  @Prop({ required: true, select: false })
  password: string;

  @Prop({ required: true, minlength: 2, maxlength: 60 })
  first_name: string;

  @Prop({ required: true, minlength: 2, maxlength: 60 })
  last_name: string;

  @Prop({
    required: true,
    unique: true,
    match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
  })
  email: string;

  @Prop({ match: /^([+]\d{2})?\d{10}$/ })
  phone_number: string;

  @Prop()
  avatar: string;

  @Prop()
  dob: Date;

  @Prop({ enum: GENDER })
  gender: string;

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Permission' }],
  })
  permission_ids: mongoose.Schema.Types.ObjectId[];

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Role' }] })
  role_ids: mongoose.Schema.Types.ObjectId[];

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'GroupPermission' }],
  })
  group_permission_ids: mongoose.Schema.Types.ObjectId[];
}

export const UserSchema = SchemaFactory.createForClass(User);
UserSchema.index({ first_name: 'text', last_name: 'text' });
UserSchema.index({ root_org_id: 1, org_id: 1 });
UserSchema.index({ created_at: 1 });
UserSchema.index({ created_by_id: 1 });
UserSchema.index({ email: 1 });
UserSchema.index({ phone_number: 1 });

export const UserFactory: ModelDefinition = {
  name: User.name,
  schema: UserSchema,
};
