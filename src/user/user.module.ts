import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { UserFactory } from './user.model';

@Module({
  controllers: [UserController],
  providers: [UserService, UserRepository],
  imports: [MongooseModule.forFeature([UserFactory])],
  exports: [UserService, UserRepository],
})
export class UserModule {}
