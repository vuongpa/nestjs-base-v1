import { Injectable } from '@nestjs/common';
import { BaseRepository } from '../base/base.repository';
import { User } from './user.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ProjectionType, QueryOptions } from 'mongoose';

@Injectable()
export class UserRepository extends BaseRepository<User> {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {
    super(userModel);
  }

  async findByEmail(email: string, projection?: ProjectionType<User> | null, options?: QueryOptions<User> | null) {
    return this.findOne({ email }, projection, options);
  }

  async findByPhoneNumber(
    phoneNumber: string,
    projection?: ProjectionType<User> | null,
    options?: QueryOptions<User> | null,
  ) {
    return this.findOne({ phoneNumber }, projection, options);
  }
}
