import { AppInstance } from '../main';
import { getConnectionToken } from '@nestjs/mongoose';

export const getModelNames = (appInstance = AppInstance) => {
  const connName = getConnectionToken();
  const conn = appInstance.get(connName);
  return Object.keys(conn.models);
};
