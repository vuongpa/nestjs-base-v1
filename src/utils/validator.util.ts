import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

const ValidatorUtil = {
  isEqual(property: string, validationOptions?: ValidationOptions) {
    return function (object: object, propertyName: string) {
      registerDecorator({
        name: 'isEqual',
        target: object.constructor,
        propertyName: propertyName,
        constraints: [property],
        options: validationOptions,
        validator: {
          validate(value: any, args: ValidationArguments) {
            const [relatedPropertyName] = args.constraints;
            const relatedValue = (args.object as any)[relatedPropertyName];
            return (
              (typeof value === 'number' || typeof value === 'string') &&
              (typeof relatedValue === 'number' || typeof relatedValue === 'string') &&
              value === relatedValue
            );
          },
        },
      });
    };
  },
  isBiggerThan(property: string, validationOptions?: ValidationOptions) {
    return function (object: object, propertyName: string) {
      registerDecorator({
        name: 'isBiggerThan',
        target: object.constructor,
        propertyName: propertyName,
        constraints: [property],
        options: validationOptions,
        validator: {
          validate(value: any, args: ValidationArguments) {
            const [relatedPropertyName] = args.constraints;
            const relatedValue = (args.object as any)[relatedPropertyName];
            return typeof value === 'number' && typeof relatedValue === 'number' && value > relatedValue;
          },
        },
      });
    };
  },
};
export default ValidatorUtil;
